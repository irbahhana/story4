from django.contrib.sites import requests
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect



# Create your views here.
def index(request):
    if request.user.is_authenticated:
        request.session['user'] = request.user.username
        request.session['email'] = request.user.email
        request.session.get('counter', 0)
        print(dict(request.session))
        for key, value in request.session.items():
            print('{} => {}'.format(key, value))
    return render(request, "library.html")


def get_data(request, find="quilting"):
    link = "https://www.googleapis.com/books/v1/volumes?q=" + find
    print(link)
    data_final = requests.get(link).json()
    return JsonResponse(data_final)


def logout(request):
    """Logs out user"""
    request.session.flush()
    return HttpResponseRedirect('/library')


def inc(request):
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] + 1
    return HttpResponseRedirect(request.session['counter'], content_type='application/json')


def dec(request):
    print(dict(request.session))
    request.session['counter'] = request.session['counter'] - 1
    return HttpResponseRedirect(request.session['counter'], content_type='application/json')
