from django.urls import path
from story9.views import index,logout,inc,dec

app_name = "story9"

urlpatterns=[
    path('', index,name="index"),
    path('logout/', logout, name='logout'),
]