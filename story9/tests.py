from django.test import TestCase, Client
from django.urls import resolve
from story9.views import index, get_data


# Create your tests here.
class Story9Test(TestCase):
    def test_landing_url_is_exist(self):  # testing whether the url exist or not
        response = Client().get('/library/')  # calling router if browser get story 9
        self.assertEqual(response.status_code, 200)  # 200 is the response if not calling error 404

    def test_landing_using_to_do_list_template(self):  #
        response = Client().get('')  # calling router
        self.assertTemplateUsed(response, 'library.html')  # to test whether the template is used in the url

    def test_landing_using_index_func(self):  # test to make sure in story 9 path using index function
        found = resolve('/library/')
        self.assertEqual(found.func, index)

    def test_landing_using_get_data_func(self):  # test to make sure in story 9 path using get data function
        found = resolve('/library/')
        self.assertEqual(found.func, get_data)

    def test_lab9_home_contain_title(self):
        title = 'Library'
        response = Client().get('/library/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)

    def test_lab9_using_table(self):
        response = Client().get('/library/')
        self.assertContains(response, 'favorite')
