$(document).ready(function () {
    ready = true;
    $("#change-theme").click(function () {
        $("#body").toggleClass('theme-1', !theme);
        $("#body").toggleClass('theme-2', theme);


        if (theme) {
            $(".accordion").css({'background-color': '#34495e', 'color': '#bdc3c7'});
            $(".panel").css('background-color', '#353b48');
            $(".notch").text('Light mode');
            $(".notch").css({'background-color': '#fff', 'color': '#34495e'});


        } else {
            $(".accordion").css({'background-color': '#dcdde1', 'color': '#34495e'});
            $(".panel").css('background-color', '#fff');
            $(".notch").text('Dark mode');
            $(".notch").css({'background-color': '#2c3e50', 'color': '#fff'});


        }
        theme = !theme;
    })


});
