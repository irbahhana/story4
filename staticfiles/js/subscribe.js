$(document).ready(function() {
    $("input").focusout(function() {
        checkEmail();
        checkAll();
    });

    $("#emailForm").keyup(function() {
        checkEmail();
    });

    $("#passwordForm").keyup(function() {
        $('#statusForm').html('');
        if ($('#passwordForm').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        } else {
            checkEmail();
        }
        checkAll();
    });

    $('#submit').click(function () {
        data = {
            'email' : $('#emailForm').val(),
            'name' : $('#nameForm').val(),
            'password' : $('#passwordForm').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value
        };
        $.ajax({
            type : 'POST',
            url : 'add_subscriber/',
            data : data,
            dataType : 'json',
            success : function(data) {
                alert(data['message']);
                document.getElementById('emailForm').value = '';
                document.getElementById('nameForm').value = '';
                document.getElementById('passwordForm').value = '';

                $('#statusForm').html('');
                checkAll();
            }
        })
    });
});

function checkEmail() {
    data = {
        'email':$('#emailForm').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    };
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function(data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#submit').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
                if ($('#passwordForm').val() !== '' && $('#passwordForm').val().length < 8) {
                    $('#statusForm').html('');
                    $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
                }
            }
        }
    });
}
function checkAll() {
    if (emailIsValid &&
        $('#nameForm').val() !== '' &&
        $('#passwordForm').val() !== '' &&
        $('#passwordForm').val().length > 7) {

        $('#submit').removeAttribute('disabled');
    } else {
        $('#submit').setAttribute('disabled', 'disabled');
    }
}
