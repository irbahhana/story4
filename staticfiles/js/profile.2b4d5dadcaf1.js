$(document).ready(function () {
    ready = true;
    $("#accordion").accordion(); //accordion

    $("#option2").click(function(){
    $('.class1').css({"background-color":"C4A69D"});
    $('.pclass1').css({"color":"black"});
    $('.class2').css({'background-color':"98A886"});
    $('h3').css({'color':'DDF8E8'});
    $('.class3').css({'background-color':"B4A6AB"});
});
    $("#option1").click(function(){
    $('.class1').css({"background-color":"black"});
    $('.pclass1').css({"color":"white"});
    $('.class2').css({'background-color':"white"});
    $('h3').css({'color':'black'});
    $('.class3').css({'background-color':"c4c4c4"});
});
    // progressbar.js@1.0.0 version is used
// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

var bar = new ProgressBar.Circle(container, {
  color: '#aaa',
  // This has to be the same size as the maximum width to
  // prevent clipping
  strokeWidth: 4,
  trailWidth: 1,
  easing: 'easeInOut',
  duration: 1400,
  text: {
    autoStyleContainer: false
  },
  from: { color: '#aaa', width: 1 },
  to: { color: '#333', width: 4 },
  // Set default step function for all animate calls
  step: function(state, circle) {
    circle.path.setAttribute('stroke', state.color);
    circle.path.setAttribute('stroke-width', state.width);

    var value = Math.round(circle.value() * 100);
    if (value === 0) {
      circle.setText('');
    } else {
      circle.setText(value);
    }

  }
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(1.0);  // Number from 0.0 to 1.0
});
