$(document).ready(function () {
    // loading screen
    function showPage() {
        $('#progressbar').css("display", "none");
        $('#dermatologist').css("display", "block");
    }

    function f() {
        var a = setTimeout(showPage, 1500);
    }

    // move();
    $(function () {
        $("#progressbar").progressbar({
            value: false
        });
        $("button").on("click", function (event) {
            var target = $(event.target),
                progressbar = $("#progressbar"),
                progressbarValue = progressbar.find(".ui-progressbar-value");

            if (target.is("#numButton")) {
                progressbar.progressbar("option", {
                    value: Math.floor(Math.random() * 100)
                });
            } else if (target.is("#colorButton")) {
                progressbarValue.css({
                    "background": '#' + Math.floor(Math.random() * 16777215).toString(16)
                });
            } else if (target.is("#falseButton")) {
                progressbar.progressbar("option", "value", false);
            }
        });

        f();

        $(function () {
            $("#accordion").accordion({
                heightStyle: "content"
            });
        });

        $("#option2").click(function () {
            $('.class1').css({"background-color": "C4A69D"});
            $('.pclass1').css({"color": "black"});
            $('.class2').css({'background-color': "98A886"});
            $('h3').css({'color': '392061'});
            $('.class3').css({'background-color': "B4A6AB"});
        });
        $("#option1").click(function () {
            $('.class1').css({"background-color": "black"});
            $('.pclass1').css({"color": "white"});
            $('.class2').css({'background-color': "white"});
            $('h3').css({'color': 'black'});
            $('.class3').css({'background-color': "c4c4c4"});
        });
        $("#organisation").click(function () {
            $.ajax({
                url: "http://localhost:8000/static/js/org.json",
                success: function (result) {
                    console.log(result);
                    for(i=0 ; i<result.length ;i++){
                        var temp = '<tr><td>' + result[i].id + '</td><td>'+ result[i].name + '</td><td>'+ result[i].detail + '</td></tr>';
                            $('#accordion').append(temp);
                    }
                }
            })
        })
        function move() {
            var elem = document.getElementById("myBar");
            var width = 10;
            var id = setInterval(frame, 10);

            function frame() {
                if (width >= 100) {
                    clearInterval(id);
                } else {
                    width++;
                    elem.style.width = width + '%';
                    elem.innerHTML = width * 1 + '%';
                }
            }
        }
    });
});
