$(document).ready(function () {
    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
        datatype: 'json',
        success: function (data) {
            for (i = 0; i < data.items.length; i++) {
                $('#bookTable').append(
                    '<tr>'
                    + '<td class=""><img src="' + data.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>'
                    + '<td class="">' + data.items[i].volumeInfo.title + '</td>'
                    + '<td class="">' + data.items[i].volumeInfo.description + '</td>'
                    + '<td class="">' + data.items[i].volumeInfo.publisher + '</td>'
                    + "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>"
                )
            }
        }
    });
    $("#submit-button").click(function() {
    var searchQuery = $("#search-query").val();
    $.ajax({

      url: "https://www.googleapis.com/books/v1/volumes?q=" + searchQuery,
      success: function (result) {
        // empty books table
        $("#bookTable tbody").html("");

        var book_count = 10;
        if (result["items"].length < 10) {
          book_count = result["items"].length
        }
        $("#books_count").text(book_count);

        // fill books table with new data
        for(var i=0; i<result.items.length; i++) {
          $('#bookTable').append(
                    '<tr>'
                    + '<td class=""><img src="' + data.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>'
                    + '<td class="">' + data.items[i].volumeInfo.title + '</td>'
                    + '<td class="">' + data.items[i].volumeInfo.description + '</td>'
                    + '<td class="">' + data.items[i].volumeInfo.publisher + '</td>'
                    + "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>"
                )
        }
      }
    });
  })
});


var counter = 0;

function favorite(clicked_id) {
    var btn = document.getElementById(clicked_id);
    if (btn.classList.contains("checked")) {
        btn.classList.remove("checked");
        document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
        counter--;
        document.getElementById("counter").innerHTML = counter;
    }
    else {
        btn.classList.add('checked');
        document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
        counter++;
        document.getElementById("counter").innerHTML = counter;
    }
}


