$(document).ready(function () {
    (function () {
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=quilting",
            success: function (data) {
                for (var i = 0; i < data.items.length; i++) {
                    $('#bookTable').append(
                        '<tr>'
                        + '<td class=""><img src="' + data.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>'
                        + '<td class="">' + data.items[i].volumeInfo.title + '</td>'
                        + '<td class="">' + data.items[i].volumeInfo.description + '</td>'
                        + '<td class="">' + data.items[i].volumeInfo.publisher + '</td>'
                        + "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>")
                }
            }
        });
    })();
});

    var counter = 0;

    function favorite(clicked_id) {
        var btn = document.getElementById(clicked_id);
        if (btn.classList.contains("checked")) {
            btn.classList.remove("checked");
            document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
            counter--;
            document.getElementById("counter").innerHTML = counter;
        }
        else {
            btn.classList.add('checked');
            document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/291/291205.svg';
            counter++;
            document.getElementById("counter").innerHTML = counter;
        }
    }

    function mencari(find) {
        var searchQuery = $("input").val();
        $.ajax({
            url: "https://www.googleapis.com/books/v1/volumes?q=" + searchQuery,
            datatype: 'json',
            success: function (find) {
                $('#bookTable').empty();
                for (var i = 0; i < find.items.length; i++) {
                    $('#bookTable').append(
                        '<tr>'
                        + '<td class=""><img src="' + find.items[i].volumeInfo.imageLinks.smallThumbnail + '"></td>'
                        + '<td class="">' + find.items[i].volumeInfo.title + '</td>'
                        + '<td class="">' + find.items[i].volumeInfo.description + '</td>'
                        + '<td class="">' + find.items[i].volumeInfo.publisher + '</td>'
                        + "<td style='padding: 15px;' class='align-middle' style='text-align:center;'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>")
                }
            }
        })
    }
    function look() {
        var word = $('input[name="pencarian"]').val();
        mencari(word);
    }