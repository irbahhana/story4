from django.shortcuts import render
from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Schedule
from .forms import Forms
from django.urls import reverse



# Create your views here.

def index(request):
    return render(request,"landing-page.html")

def profile(request):
    return render(request,"profile.html")

def register(request):
    return render(request,"register.html")

def scheduling(request):
	response = {'schedule_form' : Forms
	}
	return render(request, "formschedule.html", response)

def add_schedule(request):
	forms = Forms(request.POST)
	if(request.method == 'POST'):
		if(forms.is_valid()):
			clean = forms.cleaned_data
			schedule = Schedule()
			schedule.activity = clean['activity']
			schedule.datetime = clean['datetime']
			schedule.locations = clean['locations']
			schedule.category = clean['category']
			schedule.time = clean['time']
			schedule.save()
			return HttpResponseRedirect(reverse('story4:show'))

def show(request):
	response = {
	'schedule' : Schedule.objects.all()
	}
	return render(request,"showschedule.html",response)

def erase(request):
	Schedule.objects.all().delete()
	return HttpResponseRedirect(reverse('story4:show'))