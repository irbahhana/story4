from django.urls import path

from .views import index,profile,register,add_schedule,scheduling,show,erase
app_name = "story4"
urlpatterns = [
    path(r'',index, name="landing-page"),
    path(r'profile',profile, name="profile"),
    path(r'register',register, name="register"),
    path(r'add_schedule',add_schedule, name="add_schedule"),
    path(r'scheduling',scheduling, name="scheduling"),
    path(r'show',show,name="show"),
    path(r'erase',erase,name="erase"),
]