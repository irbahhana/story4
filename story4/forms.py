from django import forms
from django.utils import timezone

class Forms(forms.Form):
    activity_attrs = { 
        "type":"text",
        "placeholder":"Activity Name",
        "name":"activity",
        "value":"",
    }
    calendar_attrs = {
        "type":"date",
        "name":"datetime",
        "value":"",
    }
    locations_attrs = {
        "type":"text",
        "placeholder":"Location",
        "name":"location",
        "value":"",
    }
    category_attrs = {
        "type":"text",
        "placeholder":"Category",
        "name":"category",
        "value":"",
    }
    time_attrs = {
        "type":"time",
        "name":"time",
        "value":"",
    }
    activity = forms.CharField(max_length=200, required = True, widget=forms.TextInput(attrs=activity_attrs))
    datetime = forms.DateField(initial=timezone.now, required = True, widget=forms.DateInput(attrs=calendar_attrs))
    time = forms.TimeField(initial=timezone.now, required=True,widget=forms.TimeInput(attrs=time_attrs))
    locations = forms.CharField(max_length=200, required = True,widget=forms.TextInput(attrs=locations_attrs))
    category = forms.CharField(max_length=200, required = True,widget=forms.TextInput(attrs=category_attrs))