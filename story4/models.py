from django.db import models
from django.utils import timezone

# Create your models here.

class Schedule(models.Model):
    activity = models.CharField(max_length=200)
    datetime = models.DateField()
    time = models.TimeField()
    locations = models.CharField(max_length=200)
    category = models.CharField(max_length=200)
