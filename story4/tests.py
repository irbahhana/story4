from django.test import TestCase, Client
from django.urls import resolve
from .views import profile
from .views import show
from .views import register
from .views import index


# Create your tests here.
class Story4(TestCase):
    def test_story_4_url_is_exist(self):  # testing whether the url exist or not
        response = Client().get('/story4/profile')  # calling router if browser get lab-3
        self.assertEqual(response.status_code, 200)  # 200 is the response if not calling error 404

    def test_story_4_using_to_do_list_template(self):  #
        response = Client().get('/story4/profile')  # calling router
        self.assertTemplateUsed(response, 'profile.html')  # to test whether the template is used in the url

    def test_story_4_using_profile_func(self):  # test to make sure in lab-3 path using index function
        found = resolve('/story4/profile')
        self.assertEqual(found.func, profile)

    def test_story4_contains_name(self):
        response = Client().get('/story4/profile')
        self.assertContains(response,'087871786040')