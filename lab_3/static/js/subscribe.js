var emailIsValid = false;

$(document).ready(function () {
    $("input").focusout(function () {
        checkAll();
    });

    $("#email-form").keyup(function () {
        checkEmail();
    });

    $("#pass-form").keyup(function () {
        $('#statusForm').html('');
        if ($('#pass-form').val().length < 8) {
            $('#statusForm').append('<small style="color: red"> Password at least 8 character </small>');
        }
        checkAll();
    });

    $('input').focusout(function () {
        checkEmail()
    });

    $('#subs-button').click(function () {
        data = {
            'email': $('#email-form').val(),
            'name': $('#name-form').val(),
            'password': $('#pass-form').val(),
            "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
        }
        $.ajax({
            type: 'POST',
            url: 'save_subscriber/',
            data: data,
            dataType: 'json',
            success: function (data) {
                alert(data['message']);
                document.getElementById('email-form').value = '';
                document.getElementById('name-form').value = '';
                document.getElementById('pass-form').value = '';
                $('#statusForm').html('');
                $('#subs-button').prop('disabled', true);
                window.location.reload();
            }
        })
    });
    $.ajax({
        url: "list-subscriber/",
        datatype: 'json',
        success: function (data) {
            $('tbody').html('')
            var result = '<tr>';
            for (var i = 0; i < data.all_subs.length; i++) {
                result += "<th scope='row' class='align-middle text-center'>" + (i + 1) + "</th>" +
                    "<td class='align-middle text-center'>" + data.all_subs[i].name + "</td>" +
                    "<td class='align-middle text-center'>" + data.all_subs[i].email + "</td>" +
                    "<td class='align-middle text-center'>" + "<a " + "data-email=" + data.all_subs[i].email + " class='text-white btn btn-danger unsubscribe-button' float-right role='button' aria-pressed='true'>" + "Unsubscribe" + "</a></td></tr>";
            }
            $('tbody').append(result);
        },
        error: function (error) {
            alert("There's no any subcribers yet");
        }
    });

    $('#demo').on('click', 'td .unsubscribe-button', function () {
        var email = $(this).attr('data-email');
        unsubscribe(email);
    });
});

function checkEmail() {
    data = {
        'email': $('#email-form').val(),
        "csrfmiddlewaretoken": document.getElementsByName('csrfmiddlewaretoken')[0].value,
    }
    $.ajax({
        type: "POST",
        url: 'check_email/',
        data: data,
        dataType: 'json',
        success: function (data) {
            $('#statusForm').html('');
            if (data['status'] === 'fail') {
                emailIsValid = false;
                $('#subs-button').prop('disabled', true);
                $('#statusForm').append('<small style="color:red">' + data["message"] + '</small>');
            } else {
                emailIsValid = true;
                checkAll();
                $('#statusForm').append('<small style="color:green">' + data["message"] + '</small>');
            }

        }
    });
}

function checkAll() {
    if (emailIsValid &&
        $('#name-form').val() !== '' &&
        $('#pass-form').val() !== '' &&
        $('#pass-form').val().length > 7) {
        $('#subs-button').prop('disabled', false);
    } else {
        $('#subs-button').prop('disabled', true);
    }
}



