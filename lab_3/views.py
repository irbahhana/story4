from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from .forms import *
from .models import *
from django.core.validators import validate_email

# Create your views here.

response = {}


def index(request):
    response = {
        'status': Forms,
        'events': Status.objects.all()
    }
    return render(request, "to_do_list.html", response)


def add_status(request):
    forms = Forms(request.POST)
    if request.method == "POST":
        if forms.is_valid():
            clean = forms.cleaned_data
            status = Status()
            status.forms = clean['status']
            status.save()
            print(status)
            return HttpResponseRedirect('/')
    else:
        return request, 'to_do_list.html'


def bigdelete(request, id):
    deletion = get_object_or_404(Status, pk=id)
    deletion.delete()
    return HttpResponseRedirect('/')


def myprofile(request):
    return render(request, "myprofile.html")


def registro(request):
    response = {'subscribe_form': Project_Form
                }
    context = {'subscribe': 'active'}
    html = "registo.html"
    return render(request, html, response)


def check_email(request):
    try:
        print(request.POST['email'])
        validate_email(request.POST['email'])
    except:
        return JsonResponse({
            'message': 'Email format is wrong!',
            'status': 'fail'
        })

    exist = Subscriber.objects.filter(email=request.POST['email'])

    if exist:
        return JsonResponse({
            'message': 'e-mail is already exist',
            'status': 'fail'
        })

    return JsonResponse({
        'message': 'Email can be used',
        'status': 'success'
    })


def save_subscriber(request):
    if request.method == "POST":
        subscriber = Subscriber(
            email=request.POST['email'],
            name=request.POST['name'],
            password=request.POST['password']
        )
        subscriber.save()

        return JsonResponse({
            'message': 'Registration Done! Thank you'
        }, status=200)
    else:
        return JsonResponse({
            'message': "There's no GET method here!"
        }, status=403)


def listSubscriber(request):
    all_subs = Subscriber.objects.all().values()
    subs = list(all_subs)
    return JsonResponse({'all_subs': subs})
