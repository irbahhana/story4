from django.urls import path
from .views import *

# Create your views here.
app_name = "lab_3"

urlpatterns = [
    path('', index, name="index"),
    path('add_status', add_status, name="add_status"),
    path('myprofile', myprofile, name="myprofile"),
    path('registro', registro, name="registro"),
    path('check_email/', check_email, name='check_email'),
    path('save_subscriber/', save_subscriber, name='save_subscriber'),
    path('list-subscriber/', listSubscriber, name='listSubscriber'),
    path('delete/<int:id>', bigdelete, name='delete')
]
