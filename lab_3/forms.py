from django import forms
from datetime import datetime

class Forms(forms.Form):
    status_attrs = {
        "type": "text",
        "placeholder": "Hello, how are you ?",
        "class": "search-text",
        "name": "newstatus"
    }
    status = forms.CharField(max_length=200, required=True, widget=forms.TextInput(attrs=status_attrs))



class Project_Form(forms.Form):
    pass_attrs = {
        'type': 'password',
        'class': 'form-control',
        'id': 'pass-form',
        'placeholder': 'Password',
    }
    email_attrs = {
        'name': 'email',
        'type': 'text',
        'class': 'form-control',
        'id': 'email-form',
        'placeholder': 'email@example.com'
    }
    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'id': 'name-form',
        'placeholder': 'Input Your Name'

    }
    name = forms.CharField(max_length=50, error_messages={"required": "Please enter your name"},
                           widget=forms.TextInput(attrs=name_attrs))
    email = forms.EmailField(error_messages={"required": "Please enter a valid email"}, max_length=100, label='Email',
                             required=True, widget=forms.EmailInput(attrs=email_attrs))
    password = forms.CharField(label='Password', required=True, max_length=20, min_length=6,
                               widget=forms.PasswordInput(attrs=pass_attrs),
                               error_messages={"min_length": "password must be longer than 5 characters"})
