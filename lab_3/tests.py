import time
import re
from django.test import TestCase, Client, LiveServerTestCase

from selenium.webdriver.common.keys import Keys

from django.urls import resolve

from selenium import webdriver
# Create your tests here.
from lab_3.views import index, myprofile


class Lab3Test(TestCase):
    def test_landing_url_is_exist(self):  # testing whether the url exist or not
        response = Client().get('')  # calling router if browser get lab-3
        self.assertEqual(response.status_code, 200)  # 200 is the response if not calling error 404

    def test_landing_using_to_do_list_template(self):  #
        response = Client().get('')  # calling router
        self.assertTemplateUsed(response, 'to_do_list.html')  # to test whether the template is used in the url

    def test_landing_using_index_func(self):  # test to make sure in lab-3 path using index function
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_landing_url_is_exist(self):  # testing whether the url exist or not
        response = Client().get('/myprofile')  # calling router if browser get lab-3
        self.assertEqual(response.status_code, 200)  # 200 is the response if not calling error 404

    def test_if_profile_available(self):
        found = resolve('/myprofile')
        self.assertEqual(found.func, myprofile)

    def test_if_profile_contains_mynumber(self):
        response = Client().get('/myprofile')
        self.assertContains(response, '087871786040')

    def test_if_profile_using_template(self):
        response = Client().get('/myprofile')
        self.assertTemplateUsed(response, 'myprofile.html')

    def test_if_accordion_available(self):
        response = Client().get('/myprofile')
        self.assertContains(response,'Activity')


class SeleniumTest(LiveServerTestCase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName='runTest')
        self.live_server_url = None

    def setUp(self):
        self.selenium = webdriver.Chrome()
        super(SeleniumTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(SeleniumTest, self).tearDown()

    def test_automated_if_add_status_available(self):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get(self.live_server_url)
        time.sleep(10)

        masukan = selenium.find_element_by_name('status')

        # Fill the form with data
        masukan.send_keys('Foo Bar')

        # submitting the form
        masukan.send_keys(Keys.RETURN)

        # testing whether it contains foo bar using regex
        src = selenium.page_source
        text_found = re.search(r'Foo Bar', src)
        self.assertNotEqual(text_found, None)

    # check if the element title bar is available
    def test_title_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        self.assertEqual('Landing Page', selenium.title)

    # check if the element FORM for input is available
    def test_add_status_exist(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        write_position = selenium.find_element_by_name('status').get_attribute('placeholder')
        self.assertEqual('Hello, how are you ?', write_position)

    # test whether the heading is using the required style size
    def test_using_css_style_on_heading(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        heading = selenium.find_element_by_name('heading')
        heading_height = heading.value_of_css_property('height')
        self.assertEqual(heading_height,'48px')

    # test whether the heading color is using the required style
    def test_heading_color_in_css_is_red(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        tesuto = selenium.find_element_by_name('heading')
        tesuto_color = tesuto.value_of_css_property('color')
        self.assertEqual(tesuto_color, 'rgba(33, 37, 41, 1)')

    def test_card_title_placement(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        placement = selenium.find_element_by_name('cardgap').get_attribute('style')
        self.assertEqual(placement,"padding-bottom: 40px;")