from django.db import models


# Create your models here.

class Status(models.Model):
    id = models.AutoField(primary_key=True)
    forms = models.CharField(max_length=200)


class Subscriber(models.Model):
    name = models.CharField(max_length=50)
    email = models.CharField(max_length=50, unique=True)
    password = models.CharField(max_length=50)
