from django.urls import *
from .views import *

app_name='tryout'
urlpatterns = [
    path('', index,name="index"),
    path('add_item',add_items,name="add_item")
]