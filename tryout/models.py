from django.db import models
from django.utils import timezone

# Create your models here.

class Shopping(models.Model):
    items = models.CharField(max_length=100)
    datetime = models.DateTimeField(auto_now_add=True, blank=True)