"""ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
import story4.urls as story4
import story9.urls as story9

app_name="tryout2"
urlpatterns = [
    path('admin/', admin.site.urls),
    path('story4/', include((story4, story4), namespace="story4")),
    path('', include("lab_3.urls", "lab_3")),
    path('library/', include((story9, story9), namespace='story9')),
    path('auth/', include('social_django.urls', namespace='social')),
    path('tryout/', include("tryout.urls","tryout")),
    path('deathnote/', include("tryout2.urls","tryout2"))
]
