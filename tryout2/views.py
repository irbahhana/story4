from django.shortcuts import render
from .models import *
from .forms import *
from django.http import HttpResponseRedirect
# Create your views here.

def index(request):
    response = {
        'formita':Deathforms,
        'items': DeathNote.objects.all()
    }
    return render(request,'tryout2.html', response)

def add_new(request):
    forms = Deathforms(request.POST)
    if(request.method == 'POST'):
        if(forms.is_valid()):
            clean = forms.cleaned_data
            df = DeathNote()
            df.name = clean["name"]
            df.desc = clean["desc"]
            df.save()
            return HttpResponseRedirect('/deathnote')