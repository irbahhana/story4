from django.urls import path
from .views import *

app_name="tryout2"

urlpatterns = [
    path('',index,name="index"),
    path('addnew/',add_new,name="addnew")
]