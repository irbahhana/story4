from django import forms

class Deathforms(forms.Form):
    name_attrs ={
        'type':"text",
        'id':"name",
        'class':"name",
        'name':"name"
    }

    desc_attrs={
        'type':"text",
        'id':"desc",
        'class':"desc",
        'name':"desc"
    }

    name = forms.CharField(max_length=100,required=True,widget=forms.TextInput(attrs=name_attrs))
    desc = forms.CharField(widget=forms.TextInput(attrs=desc_attrs))