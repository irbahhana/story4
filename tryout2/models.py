from django.db import models

# Create your models here.

class DeathNote(models.Model):
    name = models.CharField(max_length=100)
    desc = models.TextField()
    date = models.DateField(auto_now_add=True,blank=True)